#!/usr/bin/env python3
"""
@author: mike

This is a neat syntactic sugar class that automatically takes some function
in your file and automatically executes it if run at the command line. Usage:

###
import mikelib

@mikelib.make_cmd_line
def my_func(a:int, b:int, c:str, d:list):
    # note the type annotations!
    # your code here

# At very end of file!
mikelib.make_cmd_line.run_if_cmd_line()
###

Then at the command line, you can type
    python3 ./my_file.py 0 1 2 "[3,4,5]"

which automatically calls my_func and typecasts to int, int, str, and list.

The decorator creates a new function and saves it in myfunc.cmd_line, which is
what is automatically called.

This is currently pretty primitive and doesn't work with any kwargs; if you try
to get too hip with your function declaration (e.g. using ellipses and so on)
it may not work. I'll probably evolve it later but it's pretty useful for now.
"""

#----------------------------------------------
# Header - mikelib internal version!
import sys, os
from mikelib import callable_module
#----------------------------------------------

import inspect

# This is the main decorator
def make_cmd_line(func):
    def cmd_line(args):
        # This is a new function that just calls the old one with the type
        # annotations

        # this is the the type annotation dictionary
        arg_type_dict = func.__annotations__

        # these are the arg types
        arg_types = list(arg_type_dict.values())

        # these are the types of the args
        arg_defaults = func.__defaults__

        # these are the arg parameters
        arg_parameters = inspect.signature(func).parameters

        # check to make sure we have exactly as many types as we have parameters
        if len(arg_types) != len(arg_parameters):
            raise("Every type must be type-annotated to use @make_cmd_line!")

        # now we convert every argument to its designated type
        for n in range(len(arg_types)):
            if n < len(args):
                if arg_types[n] not in [list, tuple]:
                    args[n] = arg_types[n](args[n])
                else:
                    # kind of hacky for now, but if it's a list or tuple type, we
                    # just eval it
                    args[n] = eval(args[n])
            # not needed - just call w/ args we have
            # if n >= len(args):
            #     args += [arg_defaults[n]]
        return func(*args)
    func.cmd_line = cmd_line
    # print(func)

    # Also set the __current_cmd_line__ variable in local scope, which we
    # will use to call the one of these which was most recently set (just in
    # case multiple scripts have been run that call @mikelib.make_cmd_line)
    sys._getframe(1).f_locals["__current_cmd_line__"] = func
    return func

# This runs our program if we're at the command line
def run_if_cmd_line():
    __up_frame__ = [sys._getframe(1).f_globals, sys._getframe(1).f_locals]
    __up_name__ = eval("__name__", *__up_frame__)
    __up_locals__ = sys._getframe(1).f_locals
    if "__current_cmd_line__" not in __up_locals__:
        return
    __current_cmd_line__ = __up_locals__["__current_cmd_line__"]

    if __up_name__ == "__main__":
        print("mikelib.run_if_cmd_line():")
        print("  Running from the command line!!")
        exec("import sys", *__up_frame__)
        exec(__current_cmd_line__.__name__ +".cmd_line(sys.argv[1:])",
             *__up_frame__)

#----------------------------------------------
# Footer - mikelib internal version!
callable_module.make_callable(__name__)
#----------------------------------------------
