#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##################
# Usage: from mikelib.huge_import_list import *
#
# This is a carefully curated library that autoimports as much stuff as possible
# from numpy and scipy into the global namespace without having any namespace
# collisions or redundancies. For now it also global-imports pyplot, also in
# carefully curated fashion, but I may just flip back to numpy and scipy.
#
# I also import some common libraries that I tend to import a lot
# ("sys", "os", etc), although not into the global namespace.
#
# Large docstring at the end of the file.
##################

DEBUG = True
FILENAME = "huge_import_list_static.py"
CHECK_RAW_CLASHES = False
CHECK_TRUE_CLASHES = False
CHECK_REDUNDANCIES = False
DONT_SHADOW_BUILTINS = True
IGNORE_DEPRECATED = True

from builtins import *
import re, os

old_dir = dir()
# list of modules we want to load and names we want to check


# these are things that we don't want to import from the * unless we do it on
# purpose. The first item in the tuple is the module name, the second
# is a list of names to not import
star_blacklist = [
    ("numpy", ["__version__", "show_config", "fft"]),
    ("scipy", ["__version__", "show_config", "fft"]),
    ("scipy.linalg", ["misc"]),                 # shadows scipy.misc
    ("numpy", ["sum", "any", "divmod", "all"]), # shadows builtins
    ("scipy.linalg", ["hilbert", "polar"]),     # shadows pyplot
    ("scipy.signal", ["csd", "step"]),          # shadows pyplot
]

# our main list of modules to import; this is a list of tuples.
# the first item in the tuple is the module name, so something like "numpy."
# the second item is either a string which says what you want to import the
# module as (so ("numpy", "np"), for instance) or a list of
modules = [
    # math
    ("math", "math"),
    ("math", ["pi", "tau"]),
    # numpy
    ("numpy", ["*"]),
    ("numpy", ["all as aall", "any as aany", "divmod as adivmod", "sum as asum"]),
    ("numpy", ["compat", "core", "ctypeslib", "distutils", "doc",
               "f2py", "lib", "ma", "matlib",
               "matrixlib", "polynomial", "setup", "testing",
               "tests", "typing", "version"]),
              # note we're ignoring conftest, dual, fft, linalg, and random
    ("numpy", "np"),
    # matplotlib
    ("matplotlib.pyplot", ["*"]),
    ("matplotlib", "mpl"),
    ("matplotlib.pyplot", "plt"),
    # scipy
    # ("scipy", ["*"]),
    ("scipy", "sp"),
    ("scipy", "scipy"),
    ("scipy.special", ["*"]),
    ("scipy.linalg", ["*"]),
    ("scipy.fft", ["*"]),
    ("scipy.signal", ["*"]),
    # ("scipy.linalg", ["hilbert as hilbmat", "polar as poldecomp"]),
    # ("scipy.signal", ["csd as estimate_csd"]),
    ("scipy", ["cluster", "constants", "fftpack", "integrate",
               "interpolate", "io", "linalg", "misc", "ndimage", "odr",
               "optimize", "signal", "sparse", "spatial", "special", "stats"]),
              # note we're ignoring fft because we have that as a function.
              # we can use sp.fft.___ to get the module explicitly

    # misc, and bring the builtins back in case they were shadowed
    ("re", "re"),
    ("sys", "sys"),
    ("os", "os"),
    ("time", "time"),
    ("inspect", "inspect"),
    ("string", "string"),
    ("datetime", "datetime"),
    ("bisect", "bisect"),
    ("cmath", "cmath"),
    ("decimal", "decimal"),
    ("fractions", "fractions"),
    ("random", "random"),
    ("itertools", "itertools"),
    ("functools", "functools"),
    ("operator", "operator"),
    ("pathlib", "pathlib"),
    ("shutil", "shutil"),
    ("pickle", "pickle"),
    ("io", "io"),
    ("argparse", "argparse"),
    ("subprocess", "subprocess"),
    ("json", "json"),
    ("base64", "base64"),
    ("binascii", "binascii"),
    ("pdb", "pdb"),
    ("importlib", "importlib"),
    ("pkgutil", "pkgutil"),
    ("ast", "ast"),

    # mike helper functions
    ("mikelib.convenience_functions", ["*"]),
]

tmp_import_names = [];

# exec_and_print = exec
full_filename = os.path.dirname(os.path.realpath(__file__)) + "/" + FILENAME
with open(full_filename, "w") as f:
    f.write("#!/usr/bin/env python3\n\n")
def exec_and_print(x):
    # print(x)
    with open(full_filename, "a") as f:
        f.write(x + "\n")
    exec(x, globals())

# do our imports!
name_uses = {}
deprecated = []
for module in modules:

    # convenience variables
    m_name = module[0]
    m_import_as = module[1]
    m_import_from = module[1] # these are the same, just makes it more verbose

    # We're going to want to import the module temporarily, under a tmp name
    # both because it'll help us see what we're importing and because we'll
    # use this for analytic stuff later
    tmp_import_name = "_" + m_name.replace(".", "_")
    tmp_import_names += [tmp_import_name]
    exec("import " + m_name + " as " + tmp_import_name)

    # if second entry is list, we are doing a "from ___ import ____ (as ___)"
    if isinstance(m_import_from, list):
        # now we iterate on all the things we're supposed to import
        for l in m_import_from:
            # if the entry is the special name "*"
            # rather than actually do that we look at the things we *would*
            # have imported from the module and see which are blacklisted
            if l == "*":
                # this is why we did that import from tmp_import_name above!
                # we can now look at the module's __all__ which tells us what
                # we'd be importing if we did from module import *
                if "__all__" in dir(eval(tmp_import_name)):
                    # we put this in set form in case there are duplicate
                    # entries in __all__, we don't want to import them twice
                    to_possibly_import = set(eval(tmp_import_name).__all__)

                    # now, remember that tmp_import thing? we'll be using that
                    # later to check for namespace collisions and etc.
                    # but, if we're doing "from ____ import *", there may be
                    # some entries in the module's __all__ that aren't in the
                    # dir(___) of the module when doing "import ____".
                    # so, at this point, we will look for such
                    # "missing modules" and just make a note of them for later
                    missing_modules = \
                        [x for x in to_possibly_import \
                         if x not in dir(eval(tmp_import_name))]
                    # for each missing module, we add to our tmp_import object
                    for m_missing in missing_modules:
                        missingtmp_import_name = "_missing_" + \
                                                 m_missing.replace(".", "_")
                        exec("from " + m_name + " import " + m_missing +
                             " as " + missingtmp_import_name)
                        exec(tmp_import_name + "." + m_missing + " = " +
                             missingtmp_import_name)
                        exec("del " + missingtmp_import_name)
                # if __all__ doesn't exist, we use the dir() of our module
                # instead and just ignore all variables starting with "_"
                else:
                    to_possibly_import = [x for x in dir(eval(tmp_import_name)) if x[0] != "_"]

                # ok, now we look at all of our possible imports and exclude
                # blacklisted items, builtins, etc - based on the settings at
                # the top of the script
                for n in to_possibly_import:
                    # there are a few builtins, namely "False" and "True" and
                    # "None" which appear in some modules, which break the
                    # below. This is because even if "False" is in np's dir()
                    # we can't just eval "np.False". If we see these, just skip
                    if n in ["False", "True", "None"]:
                        continue

                    # Now we check if this is deprecated, if this flag set
                    if IGNORE_DEPRECATED:
                        real_func = eval(tmp_import_name + "." + n)
                        real_doc = real_func.__doc__
                        if real_doc is not None and \
                           "deprecated and will be removed" in real_doc:
                            deprecated += [m_name + "." + n]
                            continue

                    # We also check if this is a builtin we're shadowing
                        if DONT_SHADOW_BUILTINS and n in dir(__builtins__):
                            continue

                    # Lastly, check if this is blacklisted
                    blacklist_matches = [x for x in star_blacklist
                                         if m_name == x[0] and n in x[1]]
                    if len(blacklist_matches) > 0:
                        continue

                    # if we got this far, import and add to name_uses!
                    exec_and_print("from " + m_name + " import " + n)
                    if n not in name_uses:
                        name_uses[n] = []
                    name_uses[n] += [m_name]
            else:
                # If we got here, this means we're doing an import of the form
                # 'from x import y', where y is *not* a star *.
                # Just do the usual import
                exec_and_print("from " + m_name + " import " + l)

                # We can also write ("numpy", ["all as aall"]), for instance
                # so we can check if the second argument has " as " in it
                # we set real_name to the name we're importing as (or the
                # original if we're not specifying)
                if " as " in l:
                    real_name = l.split(" as ")[1]
                    import_as = True
                else:
                    real_name = l
                    import_as = False

                # now add to name_uses. if we're importing "as," put in
                # parentheses to show that this is an altered name (e.g. asum
                # isn't really a child of numpy or whatever, just something
                # we're artificially adding). Otherwise put the real name in
                if real_name not in name_uses:
                    name_uses[real_name] = []
                if import_as:
                    name_uses[real_name] += ["("+m_name+")"]
                else:
                    name_uses[real_name] += [m_name]
    # if we got this far, then the second entry is not a list.
    # this means our import is of the form "import ______ as ______"
    # if we just want to do, for instance, import math, we treat that as
    # "import math as math"
    else:
        # do the import and add to name_uses
        exec_and_print("import " + m_name + " as " + m_import_as)
        if m_import_as not in name_uses:
            name_uses[m_import_as] = []
        name_uses[m_import_as] += ["(" + m_name + ")"]


if DEBUG:
    # now get the "raw clashes"
    # These are names that are used more than once
    if CHECK_RAW_CLASHES:
        print("\n = RAW CLASHES! = ")
        raw_clashes = {x: name_uses[x] for x in name_uses
                                    if len(name_uses[x]) > 1}
        for n in raw_clashes:
            # if 'builtins' in name_clashes[n]:
                print(n + ": " + str(raw_clashes[n]))


    # some of these "clashes" are literally the same name aliased several times
    # e.g. np.math is the same as math
    # we want to compare each pair of possible clashes to see which really
    # do differ.
    if CHECK_TRUE_CLASHES:
        print("\n = TRUE CLASHES! = ")
        true_clashes = set({})
        # n is the name we're comparing
        for n in name_uses:
            if len(name_uses[n]) == 1:
                continue

            # m1 and m2 are the items in the lists of modules that use each
            # name
            for m1 in name_uses[n]:
                for m2 in name_uses[n]:
                    # if m1 and m2 have parentheses, that means that the name in
                    # question was imported "as" n from the module m1 or m2. just
                    # remove parentheses from now
                    # m1_no_parens = re.sub(r"[\(\)]", "", m1)
                    # m2_no_parens = re.sub(r"[\(\)]", "", m2)
                    ##@ later note: bleh, this doesn't work. it tries, for
                    ##@ instance, to look for "math.math". Just skip if it has
                    ##@ parens
                    if "(" in m1 or "(" in m2:
                        continue

                    # build identifiers (e.g. np.sin)
                    identifier1 = "_" + m1 + "." + n
                    identifier2 = "_" + m2 + "." + n

                    # Now we check if they're really different (e.g. a true clash).
                    # For now, we say that they're different if
                    #   a != b and if a is not b
                    # This is kind of tricky - for instance, np.pi and math.pi are
                    # equal, but np.pi "is not" math.pi, because they're stored at
                    # different places in memory. *shrug*
                    if eval(identifier1 + " != " + identifier2 +
                            " and " + identifier1 + " is not " + identifier2):
                        # put in alphabetical order -
                        tmp = [identifier1, identifier2]
                        tmp.sort()
                        true_clashes |= {tuple(tmp)}
        for r in true_clashes:
            # if 'builtins' in name_clashes[n]:
                print(r)

    # Lastly, we check for redundancies!
    # Redundancies are where we have the same object aliased twice, e.g.
    # we have a "is" b.
    # This could involve two different names, so we check everything...
    if CHECK_REDUNDANCIES:
        redundancies = set({})
        print("\n = REDUNDANCIES! = ")
        for n1 in name_uses:
            for n2 in name_uses:
                for m1 in name_uses[n1]:
                    for m2 in name_uses[n2]:
                        # if m1 and m2 have parentheses, that means that the name in
                        # question was imported "as" n from the module m1 or m2. just
                        # remove parentheses from now
                        m1_no_parens = re.sub(r"[\(\)]", "", m1)
                        m2_no_parens = re.sub(r"[\(\)]", "", m2)

                        # build identifiers (e.g. np.sin)
                        identifier1 = "_" + m1_no_parens + "." + n1
                        identifier2 = "_" + m2_no_parens + "." + n2

                        # if the two things are literally the same, we just skip
                        # not a redundancy in that situation
                        if identifier1 == identifier2:
                            continue

                        # now, we check if they're the same
                        try:
                            if eval(identifier1 + " is " + identifier2):
                                tmp = [identifier1, identifier2]
                                tmp.sort()
                                redundancies |= {tuple(tmp)}
                        except (AttributeError, NameError):
                            # this just means that the
                            continue
        for r in redundancies:
            # if 'builtins' in name_clashes[n]:
                print(r)

# Lastly, we make sure we are only importing the keys, not the local variables
__all__ = list(name_uses.keys())

sys.modules[__name__].__doc__ = """
@author: mike

Usage: from mikelib.huge_import_list import *

This is a carefully curated library that autoimports as much stuff as possible
from numpy and scipy into the global namespace without having any namespace
collisions or redundancies. For now it also global-imports pyplot, also in
carefully curated fashion, but I may just flip back to numpy and scipy.

I also import some common libraries that I tend to import a lot
("sys", "os", etc), although not into the global namespace.

This script dynamically computes every possible thing in the various
namespaces and computes the various namespace collisions therein, as well
as the redundancies, which it writes a little report on. You can see which
modules you want to import, how you want to import them, and which items
you want to "blacklist" from an "import *" statement. You can use the report
to choose your blacklist and import settings. Then, given that, it writes
a line-by-line import of everything and saves it to
"huge_import_list_static.py", and also imports into the global namespace.

The basic idea:

Numpy and Scipy are very very useful python libraries; almost everything I do
uses these. The usual practice is to do something like

# ---
import numpy as np

my_fft = np.fft.fft(np.sin(2*np.pi*np.r_[0:4:1/44100]))
# ---

which gives you the fft of a sine wave sampled at 44.1KHz for 4 seconds.

But, it's pretty verbose to have to type np.fft.fft, np.sin, np.pi etc, all the
time. So it's pretty common for people to do things like

# ---
from numpy.fft import fft
from numpy import sin, pi, r_

my_fft = fft(sin(2*pi*r_[0:4:1/44100]))
# ---

Of course, you rarely want just the fft - you probably also want the ifft.
Likewise if you want sin, you probably also want cos, tan, maybe exp and log,
and so on. So now you have

from numpy.fft import fft, ifft
from numpy import sin, cos, tan, exp, log, pi, r_

As time goes on you tend to accumulate these little things and import them in
every program you write - maybe you want fftshift and ifftshift, fft2 or fftn
and their variants, etc.

Now, back in the day, it was pretty common for people to type things like

from numpy import *
from scipy.linalg import *
from matplotlib.pyplot import *

or even

from scipy import *

and there are plenty of vestigial hangovers from this practice all within the
numpy and scipy code. There even used to be a "pylab" module that did this
directly for you, and imported numpy and matplotlib into the global namespace.

Sadly, given the way these libraries have evolved, there are now all kinds of
namespace collisions that happen if you try to do this, which is why people
don't recommend just doing from numpy import * anymore. You also have
"redundancies" - scipy has better versions of most things in the numpy library
(better fft's, for instance), and scipy.special has many enhanced versions of
numpy functions, and so on, so you want to be clear which version you are
using. You also have that some numpy functions (like np.sum) shadow builtins
like sum, and there are all kinds of examples where this can screw things up.
Then, within the scipy library, there are also multiple instances of things
with the same name within different submodules (e.g. scipy.stats.norm and
scipy.linalg.norm).

This script goes through literally everything in numpy and scipy (and a few
other libraries, such as matplotlib.pyplot and some builtins) and reveals
any namespace collisions, duplicates, and redundancies. I took the time to
look at the "best" versions of each each functions - which usually means going
with the scipy version instead of the numpy version - and imports that best
version into the global namespace. So you have fft, ifft, etc from scipy as
global namespace functions. If you want the fft *modules*, you can still
type np.fft or sp.fft (or scipy.fft, if you prefer). If any functions shadow
builtins, I don't import them or use alternate names (so numpy's sum becomes
asum and so on). You can still always use the np.___ syntax as well.

From the numpy/scipy world, for now, I'm only importing numpy, scipy.special
(better versions of most things in numpy), scipy.linalg, scipy.fft, and
scipy.signal into the global namespace. I also import matplotlib.pyplot.
There are a few nontrivial namespace collisions; I gave pyplot and scipy
priority in general. I also import all of the submodules of numpy and scipy
(e.g. from scipy import linalg, from numpy import polynomial); there are
a few collisions (like scipy.linalg and numpy.linalg) and I went with the
scipy version for the canonical top-level 'linalg'. You can always type
np.linalg and sp.linalg as well.

I also just threw in a few imports I use a lot - math, re, sys, os, just all
kinds of stuff.
"""
