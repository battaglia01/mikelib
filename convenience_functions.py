#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May  7 00:48:43 2021

@author: mike

This is a set of some pretty useful convenience functions for numpy and pyplot
which I will add to.
"""
#----------------------------------------------
# Header - mikelib internal version!
import sys, os, numpy as np, pdb
import matplotlib.pyplot as plt
import matplotlib
from mikelib import check_spyder_import
# from mikelib import callable_module
check_spyder_import()
#----------------------------------------------

# Code goes here...

# useful complex number aliases
i = 1j
j = 1j

def cartesian(*arrays):
    """
    This returns a cartesian product of numpy arrays or lists.
    Usage:
        np.cartesian([1,2], [3,4,5])
        Out:
            array([[1, 3],
                   [1, 4],
                   [1, 5],
                   [2, 3],
                   [2, 4],
                   [2, 5]])
    """
    return np.array(np.meshgrid(*arrays)).T.reshape(-1,len(arrays))
sys.modules["numpy"].cartesian = cartesian

##############################################
# Matplotlib convenience classes
def set_size(ax, width, height):
    """
    This directly sets the size of a set of axes, even accounting for the
    border of the figure toolbars and etc.

    Usage:
        my_axes = plt.axes()

        my_axes.set_size(3, 3) # width, height in inches
    """

    l = ax.figure.subplotpars.left
    r = ax.figure.subplotpars.right
    t = ax.figure.subplotpars.top
    b = ax.figure.subplotpars.bottom
    figw = float(w)/(r-l)
    figh = float(h)/(t-b)
    ax.figure.set_size_inches(figw, figh)
sys.modules["matplotlib"].axes.Axes.set_size = set_size


##############################################
# Numpy matrix convenience classes
# quick matrix class -- m_[1,2,3,...,4,5,6] = [1 2 3;4 5 6]
class _m_class:
    """
    This is a convenience class, similar to things like np.r_ and np.c_,
    but which make it easy to make a matrix. See also v_ and h_.

    There are two main usage patterns. The first is to use a string that is
    formatted similarly to the string input from np.matrix:
        np.m_["1 2 3;4 5 6"]
        np.m_["[1 2 3;4 5 6]"]
        Out:
            array([[1, 2, 3],
                   [4, 5, 6]])

    You can also input items directly as follows:
        np.m_[1,2,3,...,4,5,6]
        Out:
            array([[1, 2, 3],
                   [4, 5, 6]])

    The ellipsis is interpreted as a row break, similar to the ";" in the
    above notation.

    You can also use this on other matrices. For instance:
        a = np.r_[1,2]
        b = np.r_[3,4]
        c = np.r_[5,6]
        d = np.r_[7,8]
        np.m_[a,b,...,c,d]
        Out:
            array([[1, 2, 3, 4],
                   [5, 6, 7, 8]])

    You can also use the slice notation from np.r_:
        np.m_[1:3, 3:5, ..., 5:7, 7:9]
        Out:
            array([[1, 2, 3, 4],
                   [5, 6, 7, 8]])

    """
    def __getitem__(self, key):
        # if they just put in one string, treat it as matrix notation
        # (but convert to ndarray)
        if isinstance(key, str):
            return np.array(np.matrix(key))

        # pdb.set_trace()
        # kind of a strange way to make sure it's a list
        if not isinstance(key, tuple):
            key = (key,)
        key = list(key)
        rows = []
        cur_start_index = 0
        for n in range(len(key)):
            if isinstance(key[n], slice):
                key[n] = np.r_[key[n]]
            if key[n] is ...:
                rows.append(np.hstack(key[cur_start_index:n]))
                cur_start_index = n+1
                continue
        rows.append(np.hstack(key[cur_start_index:]))
        # pdb.set_trace()
        return np.vstack(rows)
m_ = _m_class()
sys.modules["numpy"].m_ = m_


# quick 2D vstack
#    v_[1,2,3,4] = [1;2;3;4]
class _v_class:
    """
    This is a convenience class, similar to things like np.r_ and np.c_, which
    makes it easy to vertically stack things. The result is always at least
    a 2D array (e.g. at least a "column vector" matrix). See also m_ and h_.

    Usage:
        np.v_[1,2,3,4,5]
        Out:
            array([[1],
                   [2],
                   [3],
                   [4],
                   [5]])

    If you insert a 1D array, it is automatically promoted to a column vector.
    You can also use the slice notation for this:
        np.v_[1:6]
        Out:
            array([[1],
                   [2],
                   [3],
                   [4],
                   [5]])

    You an also put in other matrices, such as 2D row vectors, i.e. arrays of
    shape (1, N), and this will vertically stack them. See also h_ for an easy
    way to make these:
        np.v_[[[1,2,3,4,5]], [[1,2,3,4,5]]]
        np.v_[np.h_[1:6], np.h_[1:6]]
        Out:
            array([[1, 2, 3, 4, 5],
                   [1, 2, 3, 4, 5]])

    This is particularly useful if imported directly and abbreviated as
        v_[h_[1:6], h_[1:6]]
    """
    def __getitem__(self, key):
        # # pdb.set_trace()
        # # kind of a strange way to make sure it's a list
        # if not isinstance(key, tuple):
        #     key = (key,)
        # key = list(key)
        # rows = []
        # for n in range(len(key)):
        #     if isinstance(key[n], slice):
        #         key[n] = np.r_[n]
        # rows += [*key]
        # pdb.set_trace()
        # return np.vstack(rows)
        if not isinstance(key, tuple):
            key = (key,)
        return np.r_.__getitem__(tuple(['0,2,0'] + list(key)))
v_ = _v_class()
sys.modules["numpy"].v_ = v_

# quick 2D hstack class --
#    h_[1,2,3,4] = [1 2 3 4]
class _h_class:
    """
    This is a convenience class, similar to things like np.r_ and np.c_, which
    makes it easy to horizontally stack things. The result is always at least a
    2D array (e.g. a "row vector" matrix). See also m_ and v_.

    Usage:
        np.h_[1,2,3,4,5]
        Out:
            array([[1, 2, 3, 4, 5]])

    If you insert a 1D array, it is automatically promoted to a row vector.
    You can also use the slice notation for this:
        np.r_[1:6]
        Out:
            array([[1, 2, 3, 4, 5]])

    You an also put in other matrices, such as 2D col vectors, i.e. arrays of
    shape (N, 1), and this will horizontally stack them. See also c_ for an
    easy way to make these:

    np.h_[[[1],[2],[3],[4],[5]],[[1],[2],[3],[4],[5]]]
    np.h_[np.v_[1:6], np.v_[1:6]]
    Out:
        array([[1, 1],
               [2, 2],
               [3, 3],
               [4, 4],
               [5, 5]])

    This is particularly useful if imported directly and abbreviated as
        h_[v_[1:6], v_[1:6]]
    """
    def __getitem__(self, key):
        # # pdb.set_trace()
        # # kind of a strange way to make sure it's a list
        # if not isinstance(key, tuple):
        #     key = (key,)
        # key = list(key)
        # rows = []
        # for n in range(len(key)):
        #     if isinstance(key[n], slice):
        #         key[n] = np.r_[n]
        # rows += [*key]
        # pdb.set_trace()
        # return np.vstack(rows)
        if not isinstance(key, tuple):
            key = (key,)
        return np.r_.__getitem__(tuple(['1,2,1'] + list(key)))
h_ = _h_class()
sys.modules["numpy"].h_ = h_

#----------------------------------------------
# Footer - mikelib internal version!
# callable_module.make_callable(__name__)
#----------------------------------------------
