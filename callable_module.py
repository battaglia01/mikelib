#!/usr/bin/env python3
# -*- coding: utf-8 -*-
##############
# This is a simple syntactic sugar class that can make a module callable.
# See longer docstring at the end
##############

import sys, types, random

class callable_module(types.ModuleType):
    def __init__(self, name):
        self.__callable_name__ = name
        types.ModuleType.__init__(self, name)
        # or super().__init__(__name__) for Python 3
        self.__dict__.update(sys.modules[self.__callable_name__].__dict__)

    def __call__(self, *args, **kwargs):
        truncated_name = self.__callable_name__.split(".")[-1]

        # ok, so if we just call it and return, that messes up the stack as
        # we have an extra level, and we're playing with the stack, so we have
        # to call this from one level up.
        # to do this, we'll just create a temporary random hash name, assign
        # our function to that, and then call it from the higher frame
        # we also have to store the output, then copy it locally, then delete
        # the original and then return it
        __up_locals__ = sys._getframe(1).f_locals
        tmp_name_hash = '_callable_tmp_%032x' % random.randrange(16**32)
        tmp_args_hash = tmp_name_hash + "_args"
        tmp_kwargs_hash = tmp_name_hash + "_kwargs"
        tmp_output_hash = tmp_name_hash + "_output"
        __up_locals__[tmp_name_hash] = getattr(self, truncated_name)
        __up_locals__[tmp_args_hash] = args
        __up_locals__[tmp_kwargs_hash] = kwargs
        __up_locals__[tmp_output_hash] = Ellipsis
        exec("%s = %s(*%s, **%s)" % (tmp_output_hash, tmp_name_hash,
                                     tmp_args_hash, tmp_kwargs_hash),
             sys._getframe(1).f_locals, sys._getframe(1).f_globals)
        output = __up_locals__[tmp_output_hash]
        del __up_locals__[tmp_name_hash], \
            __up_locals__[tmp_args_hash], \
            __up_locals__[tmp_kwargs_hash], \
            __up_locals__[tmp_output_hash]
        return output

def make_callable(name):
    # check if name is __main__, which means we're running this as a shell
    # script
    if name == "__main__":
        return
    if name in sys.modules:
        sys.modules[name] = callable_module(name)
        sys._getframe(1).f_locals[name] = callable_module(name)

make_callable(__name__)

sys.modules[__name__].__doc__ == """
@author: mike

This is a simple syntactic sugar class that can make a module callable, so you
don't have to type

# ---
import mymodule

mymodule.mymodule("blah")       # mymodule.mymodule is annoying
mymodule.other_function("blah")
# ---

instead you can just type

# ---
import mymodule

mymodule("blah")                # calls mymodule.mymodule("blah")
mymodule.other_function("blah") # you have this as well
# ---

I got the idea for this from this StackOverflow post:
    https://stackoverflow.com/questions/1060796/callable-modules

The standard usage is, on your module page

# ---
# mymodule.py

import mikelib # add this to header

# ...
# your code here ...
# ...

mikelib.callable_module.make_callable(__name__) # at end of page
# ---

Note that callable_module is also a callable_module.
"""
