
Some useful convenience classes for Python. See the documentation for each. We have:

- Callable modules
- Some really useful convenience functions for numpy
- A carefully curated list of imports from numpy, scipy, and pyplot
- A decorator function to make a program easy to run from the command line
- An "include" function which replaces the old execfile()
- A file to check if we're calling %run in Spyder from something we really wanted to import

Make sure the mikelib directory is in your path somehow or another!

Usage:
    import mikelib
(This also automatically imports all submodules, e.g. just the one above import gives you mikelib.callable_module and so on)
