#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May  6 22:42:39 2021

@author: mike

Usage:
import mikelib
mikelib.check_spyder_import()

I like Spyder, but all of the various autoreload module stuff only works if
you import the file as a library rather than using "runfile" or "%run". This
little function checks to see if this is what has happened by checking to
see if the parent scope's __name__ is "__main__" and if we're in Spyder.
If so, it auto-imports it instead (in the parent scope).

There is another library module called make_cmd_line which automatically
calls the module's self-titled function (e.g my_module.my_module(...)) if this
is run from the command line; you will probably want to not use this if you are
also using that in Spyder.
"""

#----------------------------------------------
# Header - mikelib internal version!
import sys, os, pdb
from mikelib import callable_module
#----------------------------------------------

# This makes sure that we "import" this, for SpyderShell, rather than running it directly
def check_spyder_import():
    upframe = [sys._getframe(1).f_globals, sys._getframe(1).f_locals]
    updir = eval("dir()", *upframe)
    upfile = eval("__file__", *upframe)
    if "get_ipython" in updir and \
      eval("get_ipython()", *upframe).__class__.__name__ == "SpyderShell" and \
      eval("__name__", *upframe) == "__main__" and \
      len(sys.argv) == 1:
          # Can it cause problems
          print("mikelib.check_spyder_import():")
          print("  This was run as a script, but we really want to import it!")
          print("  Importing...")
          # pdb.set_trace()
          exec("import " + os.path.split(upfile)[1].split(".")[0],
               *upframe)
          # exec("from " + os.path.split(upfile)[1].split(".")[0] + " import *")
          sys.exit(0)

#----------------------------------------------
# Footer - mikelib internal version!
callable_module.make_callable(__name__)
#----------------------------------------------
