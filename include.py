#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  4 22:11:44 2021

@author: mike

This is just a simple function that's similar to python2's "execfile"; it just
"#include"'s another python file.

Usage:
    import include
    ...
    include("my_include.py")
"""

import sys, traceback, time
from mikelib import callable_module

def include(filename):
    with open(filename, 'r') as f:
        exec(f.read(), sys._getframe(1).f_globals, sys._getframe(1).f_locals)

callable_module.make_callable(__name__)
