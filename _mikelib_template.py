#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May  7 00:48:43 2021

@author: mike
This is my template for new mikelib modules; they are automatically callable.
"""
#----------------------------------------------
# Header - mikelib internal version!
import sys, os
from mikelib import callable_module
#----------------------------------------------

# Code goes here...




#----------------------------------------------
# Footer - mikelib internal version!
callable_module.make_callable(__name__)
#----------------------------------------------
