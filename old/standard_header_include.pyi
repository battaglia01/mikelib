import sys, os, inspect

# This makes sure that we "import" this, for SpyderShell, rather than running it directly
if "get_ipython" in dir() and \
   get_ipython().__class__.__name__ == "SpyderShell" and \
   __name__ == "__main__" and \
   len(sys.argv) == 1:
    exec("import " + os.path.split(__file__)[1].split(".")[0])
    # exec("from " + os.path.split(__file__)[1].split(".")[0] + " import *")
    sys.exit(0)
